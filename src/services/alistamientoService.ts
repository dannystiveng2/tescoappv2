import { Injectable } from "@angular/core";
import { Http, RequestOptions, Headers } from "@angular/http";
import { AlistamientoI } from "../interface/alistamiento_i";

@Injectable()
export class AlistamientoService {
  myHeaders = new Headers();
  //myParams = new URLSearchParams();
  options = null;
  constructor(private http: Http) {
    this.myHeaders.set("Content-Type", "application/x-www-form-urlencoded"); //evita que se envie la peticion options
    //this.myHeaders.set('Accept', 'text/plain');
    this.options = new RequestOptions({ headers: this.myHeaders });
  }

  buscarAlistamiento(alistamiento: AlistamientoI) {
    return this.http
      .get(
        "https://www.tescotur.com/app/index.php?action=buscarAlistamiento&al_movil=" +
          alistamiento.al_movil +
          "&al_conductor=" +
          alistamiento.al_conductor
      )
      .toPromise()
      .then(resultado => {
        return resultado.json();
      });
  }
  nuevoAlistamiento(alistamiento: AlistamientoI) {
    return this.http
      .get(
        "https://www.tescotur.com/app/index.php?action=nuevoAlistamiento&al_movil=" +
          alistamiento.al_movil +
          "&al_conductor=" +
          alistamiento.al_conductor
      )
      .toPromise()
      .then(resultado => {
        return resultado.json();
      });
  }
  fecha() {
    return this.http
      .get("https://www.tescotur.com/app/index.php?action=fecha_actual")
      .toPromise()
      .then(resultado => {
        return resultado.json();
      });
  }
  terminarAlistamiento(alistamiento: AlistamientoI) {
    return this.http
      .post(
        "https://www.tescotur.com/app/index.php?action=terminar_alistamiento",
        alistamiento,
        this.options
      )
      .toPromise()
      .then(resultado => {
        return resultado.json();
      });
  }

  emailRechazoAlistamiento(items, movil) {
    return this.http
      .post(
        "https://www.tescotur.com/app/index.php?action=email_rechazo",
        { items: items, movil: movil },
        this.options
      )
      .toPromise()
      .then(resultado => {
        return resultado.json();
      });
  }

  consultaDocumentos(n_movil, conductor, alistamiento: AlistamientoI) {
    this.http
      .get(
        "https://www.tescotur.com/app/index.php?action=documentos_vehiculo&al_movil=" +
          n_movil
      )
      .toPromise()
      .then(resultado => {
        let resultadoL = resultado.json();
        alistamiento.al_tarjeta_op = 0;
        alistamiento.al_contractual = 0;
        alistamiento.al_extracontractual = 0;
        alistamiento.al_tecnomecanica = 0;
        alistamiento.al_soat = 0;
        console.log(resultadoL);
        resultadoL.forEach(element => {
          switch (element.dv_tipo) {
            case "T.O":
              console.log("aca debe cambiar");
              alistamiento.al_tarjeta_op =
                element.dv_estado.toLowerCase() == "vigente" ? 1 : 0;
              break;
            case "RCC":
              alistamiento.al_contractual =
                element.dv_estado.toLowerCase() == "vigente" ? 1 : 0;
              break;
            case "RCE":
              alistamiento.al_extracontractual =
                element.dv_estado.toLowerCase() == "vigente" ? 1 : 0;
              break;
            case "REVISION T.M":
              alistamiento.al_tecnomecanica =
                element.dv_estado.toLowerCase() == "vigente" ? 1 : 0;
              break;
            case "SOAT":
              alistamiento.al_soat =
                element.dv_estado.toLowerCase() == "vigente" ? 1 : 0;
              break;
          }
        });
      });

    this.http
      .get(
        "https://www.tescotur.com/app/index.php?action=documentos_conductor&conductor=" +
          conductor
      )
      .toPromise()
      .then(resultado => {
        let licencia = resultado.json();

        alistamiento.al_licencia = licencia.length > 0 ? 1 : 0;
      });
    return alistamiento;
  }
}
