import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

//const headers = new Headers({'Content-Type': 'application/json'});

@Injectable()
export class VehiculosService {
  options = null;
  constructor(private http: Http) {}
  listaVehiculos(cedula) {
    return this.http
      .get(
        "https://www.tescotur.com/app/index.php?action=vehiculos&id=" + cedula
      )
      .toPromise()
      .then(resultado => {
        return resultado.json();
      });
  }
  rutasVehiculos(cedula, n_vehiculo) {
    return this.http
      .get(
        "https://www.tescotur.com/app/index.php?action=tareas_dia&id=" +
          cedula +
          "&id2=" +
          n_vehiculo
      )
      .toPromise()
      .then(resultado => {
        return resultado.json();
      });
  }
  iniciarTarea(ruta, conductor) {
    return this.http
      .post(
        "https://www.tescotur.com/app/index.php?action=saveTarea",
        JSON.stringify({
          dp_id: ruta.dp_id,
          dp_recorrido: ruta.dp_recorrido,
          vehiculo: ruta.dp_vehiculo,
          conductor: conductor.em_id
        })
      )
      .toPromise();
  }
  finalizarTareas(ruta, conductor) {
    //  console.log(JSON.stringify({"dp_id": dp_id,"dp_recorrido":dp_recorrido, "vehiculo":dp_movil,"conductor":conductor}));
    return this.http
      .post(
        "https://www.tescotur.com/app/index.php?action=actualizarTarea",
        JSON.stringify({
          dp_id: ruta.dp_id,
          dp_recorrido: ruta.dp_recorrido,
          vehiculo: ruta.dp_vehiculo,
          conductor: conductor.em_id
        })
      )
      .toPromise();
    // $http.post('', {"dp_id": dp_id,"dp_recorrido":dp_recorrido, "vehiculo":dp_movil,"conductor":sessionStorage.userId})
  }
}
