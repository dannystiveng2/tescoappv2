//import { Http } from '@angular/http';
import { Injectable, NgModule } from "@angular/core";
import "rxjs/add/operator/map";
import { AutoCompleteModule } from "ionic2-auto-complete";

@Injectable()
@NgModule({
  imports: [AutoCompleteModule]
})
export class CompleteTestService implements AutoCompleteModule {}
