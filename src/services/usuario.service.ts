import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

@Injectable()
export class UsuarioService {
  constructor(private http: Http) { }

  userById(cedula) {
    //  const header = new Headers({'Content-Type': 'application/json'});
    return this.http
      .get(
        "https://www.tescotur.com/app/index.php?action=conductor&id=" + cedula
      )
      .toPromise()
      .then(res => {
        return res.json();
      });
  }
  updateTokenApp(usuario) {
    //  const header = new Headers({'Content-Type': 'application/json'});
    return this.http
      .get(
        "https://www.tescotur.com/app/index.php?action=active_device_app&id=" +
        usuario.em_id +
        "&token_device=" +
        usuario.token_device
      )
      .toPromise()
      .then(res => {
        return res.json();
      });
  }

  iniciarSession(cod_conductor) {
    //  console.log(JSON.stringify({"dp_id": dp_id,"dp_recorrido":dp_recorrido, "vehiculo":dp_movil,"conductor":conductor}));
    return this.http
      .post(
        "https://www.tescotur.com/app/index.php?action=iniciar_sesion",
        JSON.stringify({ cod_conductor: cod_conductor })
      )
      .toPromise()
      .then(res => res);
    // $http.post('', {"dp_id": dp_id,"dp_recorrido":dp_recorrido, "vehiculo":dp_movil,"conductor":sessionStorage.userId})
  }
  cerrarSession(cod_conductor) {
    //  console.log(JSON.stringify({"dp_id": dp_id,"dp_recorrido":dp_recorrido, "vehiculo":dp_movil,"conductor":conductor}));
    return this.http
      .post(
        "https://www.tescotur.com/app/index.php?action=cerrar_sesion",
        JSON.stringify({ cod_conductor: cod_conductor })
      )
      .toPromise()
      .then(res => res);
    // $http.post('', {"dp_id": dp_id,"dp_recorrido":dp_recorrido, "vehiculo":dp_movil,"conductor":sessionStorage.userId})
  }

}
