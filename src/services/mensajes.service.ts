import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

@Injectable()
export class MensajesService {
  options = null;
  constructor(private http: Http) {}
  coodinadores() {
    return this.http
      .get("https://www.tescotur.com/app/index.php?action=coordinadores")
      .toPromise()
      .then(res => {
        return res.json();
      });
  }
  mensajes(cedula) {
    return this.http
      .get(
        "https://www.tescotur.com/app/index.php?action=mensajes&id=" + cedula
      )
      .toPromise()
      .then(res => {
        return res.json();
      });
  }
  reponderMensaje(nm_id, estado) {
    //  console.log(JSON.stringify({"dp_id": dp_id,"dp_recorrido":dp_recorrido, "vehiculo":dp_movil,"conductor":conductor}));
    return this.http
      .post(
        "https://www.tescotur.com/app/index.php?action=reponderMensaje",
        JSON.stringify({ estado: estado, nm_id: nm_id })
      )
      .toPromise()
      .then(res => res);
    // $http.post('', {"dp_id": dp_id,"dp_recorrido":dp_recorrido, "vehiculo":dp_movil,"conductor":sessionStorage.userId})
  }

  enviarMensaje(cedula, mensaje, destino) {
    return this.http
      .post(
        "https://www.tescotur.com/app/index.php?action=enviarMensaje",
        JSON.stringify({
          remitente: cedula,
          tipo: "CONDUCTOR",
          mensaje: mensaje,
          destino: destino,
          estado: "ENVIADO"
        })
      )
      .toPromise()
      .then(res => res);
  }
}
