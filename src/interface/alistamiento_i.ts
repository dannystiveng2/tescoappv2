export interface AlistamientoI {
    
    al_id?: number;
    al_movil?: string;
    al_fecha?: any;
    al_conductor?: string;
    al_licencia?: number;
    al_soat?: number;
    al_contractual?: number;
    al_extracontractual?: number;
    al_tarjeta_op?: number;
    al_tecnomecanica?: number;
    al_aceite_motor?: string;
    al_sist_frenos?: string;
    al_sist_direccion?: string;
    al_refrigerante?: string;
    al_agual_limpiabrisas?: string;
    al_baterias?: string;
    al_ajustes_tapas?: string;
    al_filtro_humedos?: string;
    al_filtros_secos?: string;
    al_estado_correas?: string;
    al_testigos_tablero?: string;
    al_disp_velocidad?: string;
    al_bornes_bateria?: string;
    al_luces_stop?: string;
    al_direccionales?: string;
    al_luz_pito_reversa?: string;
    c?: string;
    al_luces_MA?: string;
    al_eq_carretera?: string;
    al_espejos?: string;
    al_cinturones?: string;
    al_botiquin?: string;
    al_labrado?: string;
    al_rines_esp?: string;
    al_pres_aire?: string;
    al_llanta_rep?: string;
    al_gato?: string;
    al_palanca?: string;
    al_copa_ruedas?: string;
    al_conos?: string;
    al_manguera_aire?: string;
    al_extintor?: string;
    al_overol?: string;
    al_guantes?: string;
    al_dos_tacos?: string;
    al_linterna?: string;
    al_gafas?: string;
    al_alicates?: string;
    al_llave_expansion?: string;
    al_llaves_fijas?: string;
    al_desatornillador_p?: string;
    al_desatornillador_estrella?: string;
    al_estado?:string;

}