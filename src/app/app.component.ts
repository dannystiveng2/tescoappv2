import { Component } from "@angular/core";
import { Platform } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from '@ionic-native/splash-screen';
import { SessionUsuario } from "../providers/sessionUser";
import { Storage } from "@ionic/storage";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  public rootPage: any;

  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    private splashscreen: SplashScreen,
    private sessionUser: SessionUsuario,
    private storage: Storage
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      this.splashscreen.hide();
    });

    this.storage
      .get("usuario")
      .then(val => {
        if (val.em_id >= 0) {
          this.sessionUser.setUser(val);
          this.rootPage = "MenuFooterPage";
        } else {
          this.rootPage = "LoginPage";
        }
      })
      .catch(res => {

        this.rootPage = "LoginPage";
      });



    //this.rootPage = "LoginPage";
  }
  redirectSession() { }
  ionViewWillEnter() { }

  /*async showAlert(title, msg, task) {
    const alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg,
      buttons: [
        {
          text: `Action: ${task}`,
          handler: () => {
            // E.g: Navigate to a specific screen
          }
        }
      ]
    });
    alert.present();
  }*/
}
