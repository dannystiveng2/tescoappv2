import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import {
  IonicSelectableModule,
  IonicSelectableComponent
} from "ionic-selectable";

import { StatusBar } from "@ionic-native/status-bar";
import { IonicStorageModule } from "@ionic/storage";
import { SQLite } from "@ionic-native/sqlite";

import { HttpModule } from "@angular/http";
import { VehiculosService } from "../services/vehiculos.service";
import { RlTagInputModule } from "angular2-tag-input/dist";
import { MensajesService } from "../services/mensajes.service";
import { UsuarioService } from "../services/usuario.service";
import { LocalNotifications } from "@ionic-native/local-notifications";
import { TasksServiceProvider } from "../providers/tasks-service";
import { SessionUsuario } from "../providers/sessionUser";
import { SplashScreen } from '@ionic-native/splash-screen';
import { AlistamientoService } from "../services/alistamientoService";
import { SessionAlistamiento } from "../providers/sessionAlistamiento";
import { MyApp } from "./app.component";
import { OneSignal } from "@ionic-native/onesignal";

@NgModule({
  declarations: [MyApp],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot(),
    RlTagInputModule,
    IonicSelectableModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    StatusBar,
    LocalNotifications,
    SQLite,
    SplashScreen,
    VehiculosService,
    MensajesService,
    UsuarioService,
    SessionUsuario,

    AlistamientoService,
    SessionAlistamiento,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    TasksServiceProvider,
    IonicSelectableComponent,
    IonicSelectableModule,
    OneSignal
  ]
})
export class AppModule { }
