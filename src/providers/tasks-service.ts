import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';


/*
  Generated class for the TasksServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TasksServiceProvider {
  db: SQLiteObject = null;
  constructor() {
    //console.log('Hello TasksServiceProvider Provider');
  }

  setDatabase(db: SQLiteObject){
    if(this.db === null){
      this.db = db;
    }
  }

  createTable(){
    let sql = 'CREATE TABLE IF NOT EXISTS conductor (id INTEGER PRIMARY KEY AUTOINCREMENT, ITEM VARCHAR(100) , VALOR VARCHAR(100) )';
    //let sql = 'CREATE TABLE IF NOT EXISTS conductor ( em_nombre TEXT , em_id NUMERIC , codigo_cond INTEGER)';
    return this.db.executeSql(sql, []);
  }

  create(conductor: any){
 //   this.db.executeSql("delete from conductor", [])
    let sql = 'INSERT INTO conductor(ITEM,VALOR) VALUES(?,?)';
   // this.db.executeSql(sql, ["em_nombre",conductor.em_nombre])
   // this.db.executeSql(sql, ["em_id", conductor.em_id ])
    return this.db.executeSql(sql, ["codigo_cond", conductor.codigo_cond]);
  }
  getAll(){
    let sql = 'SELECT * FROM conductor  ';
    return this.db.executeSql(sql, [])
    .then(response => {
      let conductor = [];
      for (let index = 0; index < response.rows.length; index++) {
        conductor.push( response.rows.item(index) );
      }
      return Promise.resolve( conductor );
    })
    .catch(error => Promise.reject(error));
  }
}
