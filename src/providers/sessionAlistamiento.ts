import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { AlistamientoI } from "../interface/alistamiento_i";

@Injectable()
export class SessionAlistamiento {
    alistamiento: AlistamientoI;
    completoDocumento = 0;
    completoNivel = 0;
    completoMecanico = 0;
    completoEquipo = 0;
    completoLlantas = 0;
    completoCarretera = 0;
    completoHerramienta = 0;
    constructor(public storage: Storage) {

    }

    set(user) {
        //this.storage.set("user", JSON.stringify(user));
        this.alistamiento = user;
    }
    get() {
        /* let usuario;
          return  this.storage.forEach((value, key, iterationNumber) => {
             usuario = JSON.parse(value);
         }).then(()=>{
             return Promise.resolve(usuario);
         });*/
        return this.alistamiento;
    }
    eliminaSession() {
        //        this.storage.remove("user");
        this.alistamiento = {};
    }

    getOpciones() {
        return {
            opciones: [{ nombre: "Vigente", valor: "1" }, { nombre: "Vencido", valor: "0" }],
            opciones2: [{ nombre: "Bueno", valor: "B" }, { nombre: "Mal Estado", valor: "M" }, { nombre: "Permite Corrección", valor: "PC" }],
            opciones3: [{ nombre: "Si", valor: "S" }, { nombre: "No", valor: "N" }, { nombre: " N/A", valor: "N/A" }]
        }
    }

    setConteo() {

        let documentos = this.getItems().itemsDocumentos;
        let nivel = this.getItems().itemNivelFugas;
        let mecanico = this.getItems().itemsMecanico;
        let equipo = this.getItems().itemsEquipo;
        let llantas = this.getItems().itemsLlantas;
        let carretera = this.getItems().itemsCarretera;
        let herramienta = this.getItems().itemsHerramienta;
        let self = this;
        documentos = documentos.filter(function (item) { return self.alistamiento[item.campo] !== undefined; });
        nivel = nivel.filter(function (item) { return self.alistamiento[item.campo] !== undefined; });
        mecanico = mecanico.filter(function (item) { return self.alistamiento[item.campo] !== undefined; });
        equipo = equipo.filter(function (item) { return self.alistamiento[item.campo] !== undefined; });
        llantas = llantas.filter(function (item) { return self.alistamiento[item.campo] !== undefined; });
        carretera = carretera.filter(function (item) { return self.alistamiento[item.campo] !== undefined; });
        herramienta = herramienta.filter(function (item) { return self.alistamiento[item.campo] !== undefined; });


        this.completoDocumento = documentos.length;
        this.completoNivel = nivel.length;
        this.completoMecanico = mecanico.length;
        this.completoEquipo = equipo.length;
        this.completoLlantas = llantas.length;
        this.completoCarretera = carretera.length;
        this.completoHerramienta = herramienta.length;
    }
    getItems() {
        return {
            itemsDocumentos:
                [
                    { nombre: "Licencia / Pase", campo: "al_licencia" },
                    { nombre: "Soat", campo: "al_soat" },
                    { nombre: "P. Contractual", campo: "al_contractual" },
                    { nombre: "P. Extracontractual", campo: "al_extracontractual" },
                    { nombre: "Tar. de operacion", campo: "al_tarjeta_op" },
                    { nombre: "Tecnomecánica", campo: "al_tecnomecanica" }
                ],

            itemNivelFugas: [
                { nombre: "Aceite Motor", campo: "al_aceite_motor" },
                { nombre: "Sistema de Frenos", campo: "al_sist_frenos" },
                { nombre: "Sistema de Dirección", campo: "al_sist_direccion" },
                { nombre: "Refrigerante", campo: "al_refrigerante" },
                { nombre: "Agua Limpiabrisas", campo: "al_agual_limpiabrisas" },
                { nombre: "Baterias", campo: "al_baterias" },
                { nombre: "Ajuste tapas", campo: "al_ajustes_tapas" },
            ],

            itemsMecanico: [
                { nombre: "Filtros Humedos", campo: "al_filtro_humedos" },
                { nombre: "Filtros Secos", campo: "al_filtros_secos" },
                { nombre: "Estado Correas", campo: "al_estado_correas" },
                { nombre: "Testigos Tablero", campo: "al_testigos_tablero" },
                { nombre: "Disp. de Velocidad", campo: "al_disp_velocidad" },
                { nombre: "Bornes Bateria", campo: "al_bornes_bateria" },
                { nombre: "Luces Stop", campo: "al_luces_stop" },
                { nombre: "Direccionales", campo: "al_direccionales" },
                { nombre: "Luz y Pito  Reversa", campo: "al_luz_pito_reversa" },
                { nombre: "Limpiabrisas", campo: "al_limpiabrisas" },
                { nombre: "Luces Medias / Altas", campo: "al_luces_MA" },
            ],

            itemsEquipo: [
                { nombre: "Equipo Carretera", opciones: this.getOpciones().opciones2, campo: "al_eq_carretera" },
                { nombre: "Espejos", opciones: this.getOpciones().opciones2, campo: "al_espejos" },
                { nombre: "Cinturones", opciones: this.getOpciones().opciones2, campo: "al_cinturones" },
                { nombre: "Botiquín", opciones: this.getOpciones().opciones3, campo: "al_botiquin" }
            ],

            itemsLlantas: [
                { nombre: "Labrado", campo: "al_labrado" },
                { nombre: "Rines y Esparragos", campo: "al_rines_esp" },
                { nombre: "Presiones de aire", campo: "al_pres_aire" }
            ],

            itemsCarretera: [
                { nombre: "Llanta de repuesto", campo: "al_llanta_rep" },
                { nombre: "Gato", campo: "al_gato" },
                { nombre: "Palanca", campo: "al_palanca" },
                { nombre: "Copa de Ruedas", campo: "al_copa_ruedas" },
                { nombre: "Dos Señales o Conos", campo: "al_conos" },
                { nombre: "Manguera de Aire", campo: "al_manguera_aire" },
                { nombre: "Extintor", campo: "al_extintor" },
                { nombre: "Overol", campo: "al_overol" },
                { nombre: "Guantes", campo: "al_guantes" },
                { nombre: "Dos tacos", campo: "al_dos_tacos" },
                { nombre: "Linterna", campo: "al_linterna" },
                { nombre: "Gafas", campo: "al_gafas" }
            ],

            itemsHerramienta: [
                { nombre: "Alicates", campo: "al_alicates" },
                { nombre: "Lave de expansion", campo: "al_llave_expansion" },
                { nombre: "Llaves Fijas", campo: "al_llaves_fijas" },
                { nombre: "Destornillador de Pala", campo: "al_desatornillador_p" },
                { nombre: "Destornillador de Estrella", campo: "al_desatornillador_estrella" }

            ]
        };
    }
}