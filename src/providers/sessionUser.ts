import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";

@Injectable()
export class SessionUsuario {
  usuario: any;
  constructor(public storage: Storage) {}

  setUser(user) {
    //this.storage.set("user", JSON.stringify(user));
    this.storage.set("usuario", user);
    this.usuario = user;
  }
  getUser() {
    /* let usuario;
          return  this.storage.forEach((value, key, iterationNumber) => {
             usuario = JSON.parse(value);
         }).then(()=>{
             return Promise.resolve(usuario);
         });*/
    return this.usuario;
  }
  eliminaSession() {
    //        this.storage.remove("user");
    this.usuario = {};
    this.storage.remove("usuario");
  }
}
