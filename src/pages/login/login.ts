import { Component } from "@angular/core";

import {
  Platform,
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
import { UsuarioService } from "../../services/usuario.service";
import { TasksServiceProvider } from "../../providers/tasks-service";
import { SessionUsuario } from "../../providers/sessionUser";
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from "@ionic/storage";


import { OneSignal } from "@ionic-native/onesignal";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  //VARIABLES
  alistamientoD = "ConfirmaAlistamientoPage";
  cedula: number = null;
  password: any = null;
  loadingModel = null;
  ll: any = null;
  _this = this;
  dataToken: any;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    private usersService: UsuarioService,
    public loadingCtrl: LoadingController,
    public tasksService: TasksServiceProvider,
    private alertCtrl: AlertController,
    //private splashscreen: SplashScreen,
    public sessionUser: SessionUsuario,
    //private storage: Storage,
    public oneSignal: OneSignal,

  ) {


  }

  setupPush() {
    // I recommend to put these into your environment.ts
    if (this.platform.is("cordova")) {
      this.oneSignal.startInit(
        "966194ef-6f6b-49ac-b343-35f098b4811a",
        "1019907280032"
      );

      this.oneSignal.inFocusDisplaying(
        this.oneSignal.OSInFocusDisplayOption.None
      );
      this.oneSignal.getIds().then(res => {
        //actualizar el usuario device
        let info = res;
        //ACTUALIZAR TOKEN APP
        this.usersService.updateTokenApp({
          em_id: this.cedula,
          token_device: info.userId
        });
      });
      this.oneSignal.endInit();
    }
  }

  ionViewWillEnter() {

    /* this.storage.get("token_onesignal").then(val => {
       this.dataToken = val;
     });*/
  }

  inciarSesion() {
    this.loadingModel = this.loadingCtrl.create({
      content:
        ' <div class="custom-spinner-container"><div class="custom-spinner-box"></div> Iniciando Sesión...</div>'
    });
    /* CAMPOS OBLIGATORIOS */
    this.loadingModel.present();
    if (this.cedula == null && this.password == null) {
      this.loadingModel.dismiss();
      this.alertCtrl
        .create({
          title: "Ups!!",
          subTitle: "Los campos son obligatorios",
          buttons: ["Cerrar"]
        })
        .present();
    } else {
      this.usersService
        .userById(this.cedula)
        .then(response => {
          let data = response[0];
          let alerta = this.alertCtrl.create({
            title: "Ups!!",
            subTitle: "Datos Incorrectos",
            buttons: ["Cerrar"]
          });

          if (data == undefined) {
            this.loadingModel.dismiss();
            alerta.present();
          } else {
            if (this.password === data.codigo_cond) {
              this.loadingModel.dismiss();
              this.sessionUser.setUser(data);
              this.setupPush();
              this.usersService.iniciarSession(data.em_id);
              this.navCtrl.setRoot("MenuFooterPage");
            } else {
              this.loadingModel.dismiss();
              alerta.present();
            }
          }
        })
        .catch(info_error => {
          this.loadingModel.dismiss();
          this.alertCtrl
            .create({
              title: "Ups!!",
              subTitle: "Err:" + info_error,
              buttons: ["Cerrar"]
            })
            .present();
        });
    }
  }
}
