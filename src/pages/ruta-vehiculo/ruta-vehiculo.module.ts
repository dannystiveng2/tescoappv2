import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { IonicPageModule } from "ionic-angular";
import { RutaVehiculoPage } from "./ruta-vehiculo";

@NgModule({
  declarations: [RutaVehiculoPage],
  imports: [IonicPageModule.forChild(RutaVehiculoPage), CommonModule]
})
export class RutaVehiculoPageModule {}
