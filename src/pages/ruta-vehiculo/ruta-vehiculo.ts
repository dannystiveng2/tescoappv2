import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
import { VehiculosService } from "../../services/vehiculos.service";
import { SessionUsuario } from "../../providers/sessionUser";

/**
 * Generated class for the RutaVehiculoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-ruta-vehiculo",
  templateUrl: "ruta-vehiculo.html"
})
export class RutaVehiculoPage {
  rutas = [];
  loadingModel = null;
  infoUser = null;
  boton_activo = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    public vehiculoService: VehiculosService,
    public sessionUser: SessionUsuario,
    public loadingCtrl: LoadingController
  ) {
    this.loadingModel = this.loadingCtrl.create({
      content:
        ' <div class="custom-spinner-container"><div class="custom-spinner-box"></div> Cargando Vehículos...</div>'
    });
    this.loadingModel.present();
  }

  ionViewDidEnter() {
    let dp_vehiculo = this.navParams.get("dp_vehiculo");
    let info_u = this.sessionUser.getUser();
    //this.iniciarTarea = info;
    this.infoUser = info_u;
    let objRuta = [];
    let btnActivoInicio = false;
    this.vehiculoService
      .rutasVehiculos(info_u.em_id, dp_vehiculo)
      .then(response => {
        // this.rutas = response;
        //console.log(response);
        response.forEach(function(valor, llave) {
          //console.log(valor.td_estado);
          valor.disableButtonInciado = true;
          valor.disableButtonFinlizado = true;

          if (valor.td_estado == "Iniciado") {
            valor.disableButtonInciado = true;
            valor.disableButtonFinlizado = false;
          } else if (valor.td_estado == null && !btnActivoInicio) {
            btnActivoInicio = true;
            valor.disableButtonInciado = false;
            valor.disableButtonFinlizado = true;
          }

          objRuta.push({ key: llave, value: valor });
        });

        this.rutas = objRuta;
        console.log(this.rutas);
        this.loadingModel.dismiss();
      });
  }

  mostrarMapa(mapastr) {
    this.navCtrl.push("MapaPage", { mapa: mapastr });
  }

  organizaRutas(response) {
    let rutas = [];
    let bloqueActivo = 0;
    response.forEach(element => {
      let ruta = element;
      if (bloqueActivo > 0) {
        ruta.disableButtonFinlizado = true;
        ruta.disableButtonInciado = true;
        ruta.disableButtonDrak = true;
      } else {
        if (element.td_estado == null) {
          ruta.disableButtonFinlizado = true;
          ruta.disableButtonInciado = false;
          ruta.disableButtonDrak = true;
          bloqueActivo = 1;
        } else if (element.td_estado == "Iniciado") {
          ruta.disableButtonFinlizado = false;
          ruta.disableButtonInciado = true;
          ruta.disableButtonDrak = true;
          bloqueActivo = 1;
        } else if (element.td_estado == "Finalizado") {
          ruta.disableButtonFinlizado = true;
          ruta.disableButtonInciado = true;
          ruta.disableButtonDrak = true;
        }
      }
      rutas.push(ruta);
    });

    return rutas;
  }

  iniciarTareaDia(ruta) {
    let tareaLoading = this.loadingCtrl.create({
      content:
        ' <div class="custom-spinner-container"><div class="custom-spinner-box"></div>Iniciando Tarea...</div>'
    });
    tareaLoading.present();

    this.vehiculoService
      .iniciarTarea(ruta.value, this.infoUser)
      .then(response => {
        ruta.value.disableButtonInciado = true;
        ruta.value.disableButtonFinlizado = false;
        ruta.value.td_estado = "Iniciado";
        tareaLoading.dismiss();
      })
      .catch(info_error => {
        this.alertCtrl
          .create({
            title: "Ups!!",
            subTitle: "Err:" + info_error,
            buttons: ["Cerrar"]
          })
          .present();
      });
  }
  finalizarTareaDia(ruta) {
    let tareaLoading = this.loadingCtrl.create({
      content:
        ' <div class="custom-spinner-container"><div class="custom-spinner-box"></div> Finalizando Tarea</div>'
    });
    tareaLoading.present();

    this.vehiculoService
      .finalizarTareas(ruta.value, this.infoUser)
      .then(response => {
        ruta.value.disableButtonInciado = true;
        ruta.value.disableButtonFinlizado = true;
        let llave = ruta.key + 1;
        if (this.rutas[llave] != undefined) {
          this.rutas[llave].value.disableButtonInciado = false;
        }
        ruta.value.td_estado = "Finalizado";
        tareaLoading.dismiss();
      })
      .catch(info_error => {
        this.alertCtrl
          .create({
            title: "Ups!!",
            subTitle: "Err:" + info_error,
            buttons: ["Cerrar"]
          })
          .present();
      });
  }
}
