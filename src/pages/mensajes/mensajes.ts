import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  LoadingController,
  AlertController
} from "ionic-angular";
import { MensajesService } from "../../services/mensajes.service";
import { IonicSelectableComponent } from "ionic-selectable";
import { Destinatario } from "../../types/destinatario.service";
import { SessionUsuario } from "../../providers/sessionUser";

/**
 * Generated class for the MensajesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: "page-mensajes",
  templateUrl: "mensajes.html"
})
export class MensajesPage {
  opcionSeleccionada = null;
  listaMensajes = [];
  loadingModel = null;
  toast = null;
  destinatarios: Destinatario[];
  destino: Destinatario;
  mensaje = null;
  cedula = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public mensajeServicio: MensajesService,
    public sessionUser: SessionUsuario,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {
    this.loadingModel = this.loadingCtrl.create({
      content:
        ' <div class="custom-spinner-container"><div class="custom-spinner-box"></div> Cargando Modulos...</div>'
    });
    this.loadingModel.present();
    this.mensajeServicio.coodinadores().then(res => {
      this.destinatarios = res;
    });
  }
  destinatarioChange(event: {
    component: IonicSelectableComponent;
    value: any;
  }) {
    this.opcionSeleccionada = event.value;
  }
  seleccionado($event) {
    this.opcionSeleccionada = $event.us_id;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MensajesPage");
  }
  ionViewDidEnter() {
    let info_u = this.sessionUser.getUser();
    this.cedula = info_u.em_id;
    this.mensajeServicio
      .mensajes(info_u.em_id)
      .then(response => {
        this.listaMensajes = response;
        this.loadingModel.dismiss();
      })
      .catch(info_error => {});
  }

  enviar() {
    if (this.opcionSeleccionada == null || this.mensaje == null) {
      this.alertCtrl
        .create({
          title: "Ups!!",
          subTitle: "Debe digitar todos los campos.",
          buttons: ["Cerrar"]
        })
        .present();
    } else {
      this.mensajeServicio
        .enviarMensaje(this.cedula, this.mensaje, this.opcionSeleccionada.us_id)
        .then(response => {
          this.toastCtrl
            .create({
              message: "Mensaje Enviado!!",
              duration: 3000,
              position: "top"
            })
            .present();
          this.mensaje = null;
          //this.opcionSeleccionada = null;
        });
    }
  }

  responderMensajeBtn(tipo, mensajeTXT) {
    let cargando = this.loadingCtrl.create({
      content:
        ' <div class="custom-spinner-container"><div class="custom-spinner-box"></div> Cargando Modulos...</div>'
    });
    cargando.present();
    this.mensajeServicio
      .reponderMensaje(mensajeTXT.nm_id, tipo)
      .then(response => {
        mensajeTXT.nm_estado = tipo;
        cargando.dismiss();
        this.toastCtrl
          .create({
            message: "Mensaje Respodido!!",
            duration: 3000,
            position: "top"
          })
          .present();
      })
      .catch(res => {});
  }
}
