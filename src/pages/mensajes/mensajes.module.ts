import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MensajesPage } from './mensajes';
import { IonicSelectableModule } from '../../../node_modules/ionic-selectable';

@NgModule({
  declarations: [
    MensajesPage,
  ],
  imports: [
    IonicPageModule.forChild(MensajesPage),
    IonicSelectableModule
  ],
})
export class MensajesPageModule {}
