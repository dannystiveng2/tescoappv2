import { Component } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { IonicPage, NavController, NavParams } from "ionic-angular";

/**
 * Generated class for the MapaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-mapa",
  templateUrl: "mapa.html"
})
export class MapaPage {
  MapaUrl = null;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sanitizer: DomSanitizer
  ) {
    this.MapaUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
      this.navParams.get("mapa")
    );
    console.log(this.MapaUrl);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MapaPage");
  }
  abrirMapa() {
    window.open(
      "maps://?q=1j20laIYIk1u9x4ViktmAgict8XZlNx8v&ll=4.695226673712895%2C-74.16173549999996&z=12",
      "_system"
    );
  }
}
