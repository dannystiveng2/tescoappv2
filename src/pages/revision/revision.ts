import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

import { AlistamientoI } from "../../interface/alistamiento_i";
import { AlistamientoService } from "../../services/alistamientoService";
import { SessionAlistamiento } from "../../providers/sessionAlistamiento";

/**
 * Generated class for the RevisionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-revision",
  templateUrl: "revision.html"
})
export class RevisionPage {
  alistamientoModel: AlistamientoI = {};
  modulo = null;
  opciones = [];
  opciones2 = [];
  opciones3 = [];
  itemsDocumentos;
  itemNivelFugas;
  itemsMecanico;
  itemsEquipo;
  itemsLlantas;
  itemsCarretera;
  itemsHerramienta;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alistamientoServicio: AlistamientoService,
    public alistamientoSession: SessionAlistamiento
  ) {
    this.modulo = navParams.get("type");
    let opcionesS = this.alistamientoSession.getOpciones();
    let items = this.alistamientoSession.getItems();
    this.alistamientoModel = this.alistamientoSession.get();
    this.opciones = opcionesS.opciones;
    this.opciones2 = opcionesS.opciones2;
    this.opciones3 = opcionesS.opciones3;
    this.itemsDocumentos = items.itemsDocumentos;
    console.log(this.alistamientoModel);
    this.itemNivelFugas = items.itemNivelFugas;
    this.itemsMecanico = items.itemsMecanico;
    this.itemsEquipo = items.itemsEquipo;
    this.itemsLlantas = items.itemsLlantas;
    this.itemsCarretera = items.itemsCarretera;
    this.itemsHerramienta = items.itemsHerramienta;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad RevisionPage");
  }

  cancelar() {
    //this.alistamientoSession.setDocumentos();
    this.navCtrl.pop();
  }
  consola(selectedValue: any, campo: string) {
    console.log(selectedValue);
    console.log(campo);
    console.log(this.alistamientoModel);
  }
  guardarModulo() {}
}
