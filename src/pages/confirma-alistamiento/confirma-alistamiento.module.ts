import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmaAlistamientoPage } from './confirma-alistamiento';

@NgModule({
  declarations: [
    ConfirmaAlistamientoPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmaAlistamientoPage),
  ],
})
export class ConfirmaAlistamientoPageModule {}
