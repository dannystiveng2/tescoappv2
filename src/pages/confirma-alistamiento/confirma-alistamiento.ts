import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlistamientoI } from '../../interface/alistamiento_i';
import { SessionUsuario } from '../../providers/sessionUser';
import { SessionAlistamiento } from '../../providers/sessionAlistamiento';
import { AlistamientoService } from '../../services/alistamientoService';


/**
 * Generated class for the ConfirmaAlistamientoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirma-alistamiento',
  templateUrl: 'confirma-alistamiento.html',
})
export class ConfirmaAlistamientoPage {
  modelAlistamiento: AlistamientoI = {};
  conductor = {};
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public sessionUser: SessionUsuario,
    public sessionAlistamiento: SessionAlistamiento,
    public alistamientoServicio:AlistamientoService
  ) {

  }

  ionViewDidLoad() {
    this.modelAlistamiento = this.navParams.get("alistamiento");
    this.conductor = this.sessionUser.getUser();

  }
  realizarAlistamiento() {
    this.alistamientoServicio.fecha().then(response => {
      this.modelAlistamiento.al_fecha = response.fecha;
    })
    this.sessionAlistamiento.set(this.modelAlistamiento);
    this.navCtrl.push('AlistamientoPage');
  }
  cancelar() {
    this.navCtrl.pop();
  }
}
