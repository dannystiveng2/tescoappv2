import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController
} from "ionic-angular";
import { VehiculosService } from "../../services/vehiculos.service";
import { TasksServiceProvider } from "../../providers/tasks-service";
import { SessionUsuario } from "../../providers/sessionUser";
import { AlistamientoService } from "../../services/alistamientoService";
import { AlistamientoI } from "../../interface/alistamiento_i";

/**
 * Generated class for the VehiculosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-vehiculos",
  templateUrl: "vehiculos.html"
})
export class VehiculosPage {
  items = [];
  loadingModel = null;
  cedula: any = null;
  modelConductor: any = [];
  modelAlistamiento: AlistamientoI = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public vehiculoService: VehiculosService,
    public sessionUser: SessionUsuario,
    public loadingCtrl: LoadingController,
    public tasksService: TasksServiceProvider,
    public alertCtrl: AlertController,
    protected alistamientoServicio: AlistamientoService
  ) {

    this.loadingModel = this.loadingCtrl.create({
      content:
        ' <div class="custom-spinner-container"><div class="custom-spinner-box"></div> Cargando Vehículos...</div>'
    });
    this.loadingModel.present();
  }

  ionViewDidEnter() { }
  ionViewDidLoad() {

    let info_u = this.sessionUser.getUser();
    this.modelAlistamiento.al_conductor = info_u.em_id;
    this.loadingModel.dismiss();

    this.vehiculoService
      .listaVehiculos(info_u.em_id)
      .then(response => {
        this.items = response;
      })
      .catch(info_error => {
        this.loadingModel.dismiss();
        //this.storage.clear();
        //this.navCtrl.setRoot("LoginPage");
      });
  }
  cargarRutas(vehiculo) {
    this.modelAlistamiento.al_movil = vehiculo.dp_vehiculo;

    console.log(vehiculo);
    //VERIFICAR SI EL VEHICULO YA TIENE UN ALISTAMIENTO
    this.alistamientoServicio
      .buscarAlistamiento(this.modelAlistamiento)
      .then(response => {
        if (response.length == 0) {
          this.navCtrl.push("ConfirmaAlistamientoPage", {
            alistamiento: this.modelAlistamiento
          });
        } else {
          //console.log(response.al_estado);
          /*if (response[0].al_estado == "RECHAZADO") {
          let alert = this.alertCtrl.create({
            title: 'Ups!!!',
            subTitle: 'Este vehículo no puede iniciar el recorrido, por favor contáctese con el coordinador.',
            buttons: ['Dismiss']
          });
          alert.present();
        } else {
        }*/
          this.navCtrl.push("RutaVehiculoPage", vehiculo);
        }
      });
  }
}
