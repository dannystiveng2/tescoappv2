import { Component } from "@angular/core";
import { Platform, IonicPage, NavController, NavParams } from "ionic-angular";


import { LocalNotifications } from "../../../node_modules/@ionic-native/local-notifications";
import { MensajesService } from "../../services/mensajes.service";
import { SessionUsuario } from "../../providers/sessionUser";
import { OneSignal } from "@ionic-native/onesignal";
import { UsuarioService } from '../../services/usuario.service';

/**
 * Generated class for the MenuFooterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-menu-footer",
  templateUrl: "menu-footer.html"
})
export class MenuFooterPage {
  TabRootMensajes = "MensajesPage";
  TabRootVehiculo = "VehiculosPage";
  TabRootMapa = "MapaPage";
  intervalo = null;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    private localNotifications: LocalNotifications,
    public mensajeServicio: MensajesService,
    public navParams: NavParams,
    private oneSignal: OneSignal,
    public sessionUser: SessionUsuario,
    public servicioUsuario: UsuarioService
  ) {

    this.setupPush();

  }

  ionViewDidLoad() { }

  cantidadMensajes() {
    let info_u = this.sessionUser.getUser();
    this.mensajeServicio
      .mensajes(info_u.em_id)
      .then(response => {
        if (response.length > 0) {
          this.localNotifications.schedule({
            text: "Tienes " + response.length + " mensajes sin leer.",
            trigger: { at: new Date(new Date().getTime() + 3600) },
            led: "FF0000",
            sound: null
          });
        }
      })
      .catch(info_error => { });
  }

  logout() {
    clearInterval(this.intervalo);
    let info_u = this.sessionUser.getUser();

    this.servicioUsuario.cerrarSession(info_u.em_id);
    this.sessionUser.eliminaSession();
    this.navCtrl.setRoot("LoginPage");
  }

  setupPush() {
    if (this.platform.is("cordova")) {
      // I recommend to put these into your environment.ts
      this.oneSignal.startInit(
        "966194ef-6f6b-49ac-b343-35f098b4811a",
        "1019907280032"
      );

      this.oneSignal.inFocusDisplaying(
        this.oneSignal.OSInFocusDisplayOption.None
      );

      // Notifcation was received in general
      this.oneSignal.handleNotificationReceived().subscribe(data => {
        /*let msg = data.payload.body;
        let title = data.payload.title;
        let additionalData = data.payload.additionalData;*/
        //this.showAlert(title, msg, additionalData.task);
      });

      // Notification was really clicked/opened
      this.oneSignal.handleNotificationOpened().subscribe(data => {
        // Just a note that the data is a different place here!
        //let additionalData = data.notification.payload.additionalData;
        this.navCtrl.push("MensajesPage");
      });

      this.oneSignal.endInit();
    }
  }
}
