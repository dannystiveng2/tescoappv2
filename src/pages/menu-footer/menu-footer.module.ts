import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuFooterPage } from './menu-footer';

@NgModule({
  declarations: [
    MenuFooterPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuFooterPage),
  ],
})
export class MenuFooterPageModule {}
