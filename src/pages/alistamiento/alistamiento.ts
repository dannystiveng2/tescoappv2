import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from "ionic-angular";
import { SessionAlistamiento } from "../../providers/sessionAlistamiento";
import { SessionUsuario } from "../../providers/sessionUser";
import { AlistamientoService } from "../../services/alistamientoService";

/**
 * Generated class for the AlistamientoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-alistamiento",
  templateUrl: "alistamiento.html"
})
export class AlistamientoPage {
  usuario = null;
  modelAlistamiento = null;
  itemsDocumentos;
  itemNivelFugas;
  itemsMecanico;
  itemsEquipo;
  itemsLlantas;
  itemsCarretera;
  itemsHerramienta;

  completoDocumento = 6;
  completoNivel = 0;
  completoMecanico = 0;
  completoEquipo = 0;
  completoLlantas = 0;
  completoCarretera = 0;
  completoHerramienta = 0;
  load = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public sessionAlistamiento: SessionAlistamiento,
    public sessionUsuario: SessionUsuario,
    private alertCtrl: AlertController,
    public alistamientoServicio: AlistamientoService
  ) {
    this.usuario = this.sessionUsuario.getUser();
    this.modelAlistamiento = this.sessionAlistamiento.get();
    let items = this.sessionAlistamiento.getItems();

    this.itemsDocumentos = items.itemsDocumentos;

    this.itemNivelFugas = items.itemNivelFugas;
    this.itemsMecanico = items.itemsMecanico;
    this.itemsEquipo = items.itemsEquipo;
    this.itemsLlantas = items.itemsLlantas;
    this.itemsCarretera = items.itemsCarretera;
    this.itemsHerramienta = items.itemsHerramienta;

    this.alistamientoServicio.consultaDocumentos(
      this.modelAlistamiento.al_movil,
      this.modelAlistamiento.al_conductor,
      this.modelAlistamiento
    );
  }

  ionViewCanEnter() {
    this.sessionAlistamiento.setConteo();
    this.completoDocumento = this.sessionAlistamiento.completoDocumento;
    this.completoNivel = this.sessionAlistamiento.completoNivel;
    this.completoMecanico = this.sessionAlistamiento.completoMecanico;
    this.completoEquipo = this.sessionAlistamiento.completoEquipo;
    this.completoLlantas = this.sessionAlistamiento.completoLlantas;
    this.completoCarretera = this.sessionAlistamiento.completoCarretera;
    this.completoHerramienta = this.sessionAlistamiento.completoHerramienta;
  }
  verificarModulo(modulo) {
    this.navCtrl.push("RevisionPage", { type: modulo });
  }
  buscarItemsNegativos(Items) {
    //BUSCA ITEMS QUE NO PREMITEN LA SALIDA DEL VEHICULO
    let modelo = this.modelAlistamiento;
    let conteo = Items.filter(function (key) {
      // console.log(modelo[key.campo]);
      if (modelo[key.campo] !== undefined) {
        if (
          modelo[key.campo] == "0" ||
          modelo[key.campo] == "M" ||
          modelo[key.campo] == "N"
        ) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    });

    return conteo;
  }
  terminarAlistamiento() {
    this.load = true;
    let itemsNegativos = [];
    let totalRespuestas =
      this.completoDocumento +
      this.completoNivel +
      this.completoMecanico +
      this.completoEquipo +
      this.completoLlantas +
      this.completoCarretera +
      this.completoHerramienta;
    let totalPreguntas =
      this.itemsDocumentos.length +
      this.itemNivelFugas.length +
      this.itemsMecanico.length +
      this.itemsEquipo.length +
      this.itemsLlantas.length +
      this.itemsCarretera.length +
      this.itemsHerramienta.length;
    let itemSesion = this.sessionAlistamiento.getItems();

    if (totalRespuestas < totalPreguntas) {
      let alert = this.alertCtrl.create({
        title: "Ups!!!",
        subTitle: "Los items no están completos.",
        buttons: ["Dismiss"]
      });
      alert.present();
      this.load = false;
    } else {
      //VERIFICAR SI PUEDE ARRANCAR EL VEHICULO
      let conteoNegativos =
        this.buscarItemsNegativos(itemSesion.itemsDocumentos).length +
        this.buscarItemsNegativos(this.itemNivelFugas).length +
        this.buscarItemsNegativos(this.itemsMecanico).length +
        this.buscarItemsNegativos(this.itemsEquipo).length +
        this.buscarItemsNegativos(this.itemsLlantas).length +
        this.buscarItemsNegativos(this.itemsCarretera).length +
        this.buscarItemsNegativos(this.itemsHerramienta).length;
      if (conteoNegativos > 0) {
        this.modelAlistamiento.al_estado = "CON PENDIENTES";
        //ENVIO DE EMAIL A COODINADORES
        itemsNegativos.push(
          this.buscarItemsNegativos(itemSesion.itemsDocumentos)
        );
        itemsNegativos.push(this.buscarItemsNegativos(this.itemNivelFugas));
        itemsNegativos.push(this.buscarItemsNegativos(this.itemsMecanico));
        itemsNegativos.push(this.buscarItemsNegativos(this.itemsEquipo));
        itemsNegativos.push(this.buscarItemsNegativos(this.itemsLlantas));
        itemsNegativos.push(this.buscarItemsNegativos(this.itemsCarretera));
        itemsNegativos.push(this.buscarItemsNegativos(this.itemsHerramienta));

        this.alistamientoServicio.emailRechazoAlistamiento(
          itemsNegativos,
          this.modelAlistamiento.al_movil
        );
        /**/
      } else {
        this.modelAlistamiento.al_estado = "APROBADO";
      }


      this.alistamientoServicio
        .terminarAlistamiento(this.modelAlistamiento)
        .then(response => {
          this.load = true;
          this.navCtrl.setRoot("RutaVehiculoPage", {
            dp_vehiculo: this.modelAlistamiento.al_movil
          });
        });
    }
  }
}
