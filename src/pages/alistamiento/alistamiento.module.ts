import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AlistamientoPage } from './alistamiento';

@NgModule({
  declarations: [
    AlistamientoPage,
  ],
  imports: [
    IonicPageModule.forChild(AlistamientoPage),
  ],
})
export class AlistamientoPageModule {}
